## Create the elastic search cluster
```bash
aws cloudformation create-stack --stack-name boq-site --template-body file:///$PWD/appsynces.yml --parameters Tier=development --capabilities CAPABILITY_NAMED_IAM
```

## Install elastic search locally
```bash
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.1.1
```

## Delete the index
```bash
curl -X DELETE "localhost:9200/books" -H 'Content-Type: application/json'
```

## Set up the nested objects
```bash
curl -X PUT "localhost:9200/books"
curl -X PUT "localhost:9200/books/_mapping?pretty" -H 'Content-Type: application/json' -d'
{
  "properties": {
    "id": {
      "type": "keyword"
    }
    "title": {
      "type": "text",
      "boost": 2
    },
    "pages": {
      "type": "short",
      "index": false
    },
    "publishYear": {
      "type": "short"
    },
    "tags": {
      "type": "text",
      "analyzer": "standard",
      "fielddata": true
    },
    "formats": {
      "type": "nested" 
    },
    "users": {
      "type": "nested",
      "properties": {
        "name": {
          "type": "text",
          "analyzer": "whitespace",
          "fielddata": true
        }
      }
    }
  }
}
'
```

## Add book
```bash
curl -X PUT "localhost:9200/books/_doc/1?pretty&refresh" -H 'Content-Type: application/json' --data-binary "@book1.json"

curl -X PUT "localhost:9200/books/_doc/2?pretty&refresh" -H 'Content-Type: application/json' --data-binary "@book2.json"
```

## Search for all books
```bash
curl -X GET "localhost:9200/books/_search"
```

## Search for books requested this quarter
```bash
curl -X GET "localhost:9200/books/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": {
    "nested": {
      "path": "users",
      "query": {
        "bool": {
          "must": [
            { "match": { "users.quarter": "1Q2020" }}
          ]
        }
      }
    }
  }
}
'
```

## Search for books with the phrase 'scale'
```bash
curl -X GET "localhost:9200/books/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": {
    "bool": {
      "minimum_should_match" : 1,
      "should": [
        { 
          "multi_match": {
            "query": "scale",
            "fields": ["title", "description", "authors", "tags", "suggested_by"]
          }
        },
        {
          "nested": {
            "path": "users",
            "query": {
              "multi_match": {
                "query": "scale",
                "fields": ["users.username", "users.name"]
              }
            }
          }
        }
      ],
      "filter": {
        "nested": {
          "path": "users",
          "query": {
            "match": { 
              "users.quarter": "1Q2020"
            }
          }
        }
      }
    }
  },
  "highlight": {
    "fields": {
      "title": {},
      "description": {},
      "tags": {},
      "authors": {}
    }
  }
}
'
```


## Add a user to a book
```bash
curl -X POST "localhost:9200/books/_update/1"  -H 'Content-Type: application/json' -d'
{
    "script" : {
        "source": "ctx._source.users.add(params.user)",
        "lang": "painless",
        "params": {
            "user": {
                "username": "foong",
                "name": "Adrian Foong",
                "quarter": "1Q2020",
                "priority": 2,
                "will_lead": false,
                "location": "Lehi"
            }
        }
    }
}
'
```

## Remove user from a book
```bash
curl -X POST "localhost:9200/books/_update/1?pretty"  -H 'Content-Type: application/json' -d'
{
    "script" : {
        "source": "for (int i = 0; i < ctx._source.users.size(); i++){if(ctx._source.users[i].username==params.username){ctx._source.users.remove(i)}}",
        "lang": "painless",
        "params": {
            "username": "foong"
        }
    }
}
'
```

## Top Tags Facet Query
```bash
curl -X GET "localhost:9200/books/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "size": 0,
  "aggs" : {
    "tag_counts" : {
      "terms" : {
        "field" : "tags",
        "size": 10
      }
    }
  }
}
'
```
