
AWSTemplateFormatVersion: 2010-09-09

Parameters:
  Tier:
    Default: development
    Description: Determines which resources to deploy to account.
    Type: String
    AllowedValues:
    - development
  ESDomainName:
    Default: appsync-domain
    Description: Name of the domain.
    Type: String
    MinLength: 1
    MaxLength: 28
    AllowedPattern: '^[a-z][a-z0-9-]*$'
  GraphQLApiName:
    Default: appsync-api
    Description: Name of the AppSync GraphQL Api
    Type: String
    MinLength: 1
  ApiKeyDescription:
    Default: AppSync ApiKey
    Description: Name of the AppSync ApiKey
    Type: String

Mappings:
  ESConfig:
    development:
      ElasticsearchClusterInstanceType: t2.small.elasticsearch
      ElasticsearchClusterInstanceCount: 1
      ElasticsearchEBSVolumeSize: 10
      ElasticsearchVersion: 7.1

Resources:

  ############################
  #   IAM Roles & Policies   #
  ############################

  AppSyncESServiceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: AppSyncESServiceRole
      ManagedPolicyArns:
        - Ref: AppSyncElasticsearchPolicy
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Action:
            - sts:AssumeRole
            Effect: Allow
            Principal:
              Service: 
                - "appsync.amazonaws.com"
    DependsOn:
      - AppSyncElasticsearchPolicy
 
  AppSyncElasticsearchPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties: 
      Description: "Managed Policy for managing elasticsearch domain for AppSync."
      Path: "/appsync/elasticsearch/"
      PolicyDocument: 
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Action:
              - es:ESHttpDelete
              - es:ESHttpHead
              - es:ESHttpGet
              - es:ESHttpPost
              - es:ESHttpPut
            Resource:
              -
                Fn::Join:
                  - ""
                  -
                    - "arn:aws:es:"
                    -
                      Ref: "AWS::Region"
                    - ":"
                    -
                      Ref: "AWS::AccountId"
                    - ":domain/"
                    -
                      Ref: ESDomainName
                    - "/*"

  #################
  # Elasticsearch #
  #################
 
  AppSyncElasticSearch:
    Type: 'AWS::Elasticsearch::Domain'
    DependsOn:
      - AppSyncESServiceRole
    Properties:
      DomainName: 
          Ref: ESDomainName
      ElasticsearchVersion:
        Fn::FindInMap:
            - ESConfig
            - Ref: Tier
            - ElasticsearchVersion
      AccessPolicies:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Action:
              - es:ESHttpDelete
              - es:ESHttpHead
              - es:ESHttpGet
              - es:ESHttpPost
              - es:ESHttpPut
            Principal:
              AWS:
                - Fn::Sub: "arn:aws:iam::${AWS::AccountId}:role/AppSyncESServiceRole"
            Resource:
              -
                Fn::Join:
                  - ""
                  -
                    - "arn:aws:es:"
                    -
                      Ref: "AWS::Region"
                    - ":"
                    -
                      Ref: "AWS::AccountId"
                    - ":domain/"
                    -
                      Ref: ESDomainName
                    - "/*"
      AdvancedOptions:
        "indices.fielddata.cache.size": "40"
        "rest.action.multi.allow_explicit_index": "true"
      ElasticsearchClusterConfig:
        ZoneAwarenessEnabled: "false"
        InstanceType: 
          Fn::FindInMap:
            - ESConfig
            - Ref: Tier
            - ElasticsearchClusterInstanceType
        InstanceCount:
          Fn::FindInMap:
            - ESConfig
            - Ref: Tier
            - ElasticsearchClusterInstanceCount
      EBSOptions: 
        EBSEnabled: true
        Iops: 0
        VolumeSize:
          Fn::FindInMap:
            - ESConfig
            - Ref: Tier
            - ElasticsearchEBSVolumeSize
        VolumeType: "gp2"

  #####################
  # AppSync Resources #
  #####################

  AppSyncGraphQLApi:
    Type: "AWS::AppSync::GraphQLApi"
    DependsOn:
      - AppSyncElasticSearch
    Properties:
      Name: !Ref GraphQLApiName
      AuthenticationType: "API_KEY"

  AppSyncApiKey:
    Type: "AWS::AppSync::ApiKey"
    DependsOn:
      - AppSyncGraphQLApi
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      Description:
        Ref: ApiKeyDescription

  AppSyncSchema:
    Type: "AWS::AppSync::GraphQLSchema"
    DependsOn:
      - AppSyncGraphQLApi
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      Definition: |
        schema {
          query: Query
          mutation: Mutation
        }
        type Book {
          authors: [String]
          description: String
          formats: [BookLink]
          id: ID!
          pages: Int
          publishYear: Int
          suggested_by: String
          tags: [String]
          title: String!
          users: [User]
        }
        type BookLink {
          format: Format
          link: String
        }
        type Mutation {
          # # If an item exists it's updated. If it does not it's created.
          book(book: BookInput!): Book
          deleteBook(id: ID): Boolean
          init(complete: Boolean): Boolean
          subscribe(id: ID, user: UserInput): User
          unsubscribe(id: ID, user: UserInput): User
        }
        type Query {
          #  Get a single value of type 'Book' by primary key.
          book(id: ID!): Book
          #  Get a list of books, optionally filtered by an es query
          books(quarter: String, query: String): [Book]
        }
        type User {
          format: Format
          location: String
          name: String
          priority: Int
          quarter: String
          username: String
          will_lead: Boolean
        }
        enum Format {
          AUDIBLE
          BOOK
          KINDLE
          SAFARI
        }
        input BookInput {
          authors: [String]
          description: String
          formats: [BookLinkInput!]
          id: ID!
          pages: Int
          publishYear: Int
          suggested_by: String
          tags: [String]
          title: String!
          users: [UserInput]
        }
        input BookLinkInput {
          format: Format!
          link: String!
        }
        input UserInput {
          format: Format!
          location: String!
          name: String
          priority: Int
          quarter: String!
          username: String!
          will_lead: Boolean
        }

  AppSyncDataSource:
    Type: "AWS::AppSync::DataSource"
    DependsOn:
      - AppSyncGraphQLApi
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      Name: "esdatasource"
      Description: "A DataSource for Elasticsearch."
      Type: "AMAZON_ELASTICSEARCH"
      ServiceRoleArn: !GetAtt AppSyncESServiceRole.Arn
      ElasticsearchConfig:
        AwsRegion: !Ref "AWS::Region"
        Endpoint: !Join ["", ["https://", !GetAtt AppSyncElasticSearch.DomainEndpoint]]

  CreateIndexFunction:
    Type: "AWS::AppSync::FunctionConfiguration"
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      DataSourceName: !GetAtt AppSyncDataSource.Name
      FunctionVersion: '2018-05-29'
      Name: "create_index"
      RequestMappingTemplate: |
        #**
        The 'params' key accepts any valid Elasticsearch DSL expression.
        You must replace the <index>, <type>, and <field> placeholders with actual values.
        *#
        {
          "operation":"PUT",
          "path":"/books",
          "params":{}
        }
      ResponseMappingTemplate: |
        ## Raise a GraphQL field error in case of a datasource invocation error
        #if($ctx.error)
            $util.error($ctx.error.message, $ctx.error.type)
        #end
        #**
        $context.result contains the full response of the Elasticsearch query.
        Select a subset of information or iterate through hits to return the
        same shape as is expected by this field.
        *#
        $util.toJson(true)

  InitBooksMappingFunction:
    Type: "AWS::AppSync::FunctionConfiguration"
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      DataSourceName: !GetAtt AppSyncDataSource.Name
      FunctionVersion: '2018-05-29'
      Name: "init_books_mapping"
      RequestMappingTemplate: |
        {
          "operation": "PUT",
          "path": "/books/_mapping",
          "params": {
            "body": {
              "properties": {
                "id": {
                  "type": "keyword"
                },
                "title": {
                  "type": "text",
                  "boost": 2
                },
                "pages": {
                  "type": "short",
                  "index": false
                },
                "publishYear": {
                  "type": "short"
                },
                "tags": {
                  "type": "text",
                  "analyzer": "standard",
                  "fielddata": true
                },
                "formats": {
                  "type": "nested"
                },
                "users": {
                  "type": "nested",
                  "properties": {
                    "name": {
                      "type": "text",
                      "analyzer": "whitespace",
                      "fielddata": true
                    }
                  }
                }
              }
            }
          }
        }
      ResponseMappingTemplate: |
        ## Raise a GraphQL field error in case of a datasource invocation error
        #if($ctx.error)
            $util.error($ctx.error.message, $ctx.error.type)
        #end
        #**
        $context.result contains the full response of the Elasticsearch query.
        Select a subset of information or iterate through hits to return the
        same shape as is expected by this field.
        *#
        $util.toJson(true)

  DeleteIndexFunction:
    Type: "AWS::AppSync::FunctionConfiguration"
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      DataSourceName: !GetAtt AppSyncDataSource.Name
      FunctionVersion: '2018-05-29'
      Name: "delete_index"
      RequestMappingTemplate: |
        #**
        The 'params' key accepts any valid Elasticsearch DSL expression.
        You must replace the <index>, <type>, and <field> placeholders with actual values.
        *#
        {
          "operation":"DELETE",
          "path":"/books",
          "params":{}
        }
      ResponseMappingTemplate: |
        ## Raise a GraphQL field error in case of a datasource invocation error
        #if($ctx.error)
            $util.error($ctx.error.message, $ctx.error.type)
        #end
        #**
        $context.result contains the full response of the Elasticsearch query.
        Select a subset of information or iterate through hits to return the
        same shape as is expected by this field.
        *#
        $util.toJson(true)


  BookResolver:
    Type: "AWS::AppSync::Resolver"
    DependsOn:
      - AppSyncSchema
      - AppSyncDataSource
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      TypeName: "Mutation"
      FieldName: "book"
      DataSourceName: !GetAtt AppSyncDataSource.Name
      RequestMappingTemplate: |
        {
        "version": "2017-02-28",
        "operation": "PUT",
        "path":"/books/_doc/${context.args.book.id}",
        "params":{
            "headers":{},
            "queryString":{},
            "body":$util.toJson($context.args.book)
          }
        }
      ResponseMappingTemplate: |
        #if( $context.result.error )
          $utils.error("Failed to update book", "ElasticSearchError", $context.result)
        #else
          $util.toJson($context.args.book)
        #end
      Kind: "UNIT"

  DeleteBookResolver:
    Type: "AWS::AppSync::Resolver"
    DependsOn:
      - AppSyncSchema
      - AppSyncDataSource
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      TypeName: "Mutation"
      FieldName: "deleteBook"
      DataSourceName: !GetAtt AppSyncDataSource.Name
      RequestMappingTemplate: |
        #**
            Use the 'path' to get a single value by id.
            You must replace the <index> and <type> placeholder with actual values.
        *#
        {
            "version": "2017-02-28",
            "operation": "DELETE",
            "path": "/books/_doc/${context.arguments.id}",
            "params": {}
        }
      ResponseMappingTemplate: |
        #**
            $context.result contains the full response of the Elasticsearch query.
            You can use this template to return the data from an Elasticsearch
            query that returns a single result.
        *#
        $util.toJson($context.result.get("_id"))
      Kind: "UNIT"

  InitResolver:
    Type: "AWS::AppSync::Resolver"
    DependsOn:
      - AppSyncSchema
      - AppSyncDataSource
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      TypeName: "Mutation"
      FieldName: "init"
      RequestMappingTemplate: |
        {}
      ResponseMappingTemplate: |
        ## The after mapping template is used to collect the final value that is returned by the resolver.
        $util.toJson($ctx.result)
      Kind: "PIPELINE"
      PipelineConfig:
        Functions:
          - !GetAtt CreateIndexFunction.FunctionId
          - !GetAtt InitBooksMappingFunction.FunctionId
          - !GetAtt DeleteIndexFunction.FunctionId

  SubscribeResolver:
    Type: "AWS::AppSync::Resolver"
    DependsOn:
      - AppSyncSchema
      - AppSyncDataSource
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      TypeName: "Mutation"
      FieldName: "subscribe"
      DataSourceName: !GetAtt AppSyncDataSource.Name
      RequestMappingTemplate: |
        #**
        The 'params' key accepts any valid Elasticsearch DSL expression.
        You must replace the <index>, <type>, and <field> placeholders with actual values.
        *#
        {
          "version":"2017-02-28",
          "operation":"POST",
          "path":"/books/_update/${context.arguments.id}",
          "params":{
              "body": {
                  "script" : {
                      "source": "ctx._source.users.add(params.user)",
                      "lang": "painless",
                      "params": {
                          "user": $util.toJson(${context.arguments.user})
                      }
                  }
              }
          }
        }
      ResponseMappingTemplate: |
        #**
        $context.result contains the full response of the Elasticsearch query.
        You can use this template to return the data from an Elasticsearch
        query that returns a single result.
        *#
        $util.toJson(${context.arguments.user})
      Kind: "UNIT"

  UnsubscribeResolver:
    Type: "AWS::AppSync::Resolver"
    DependsOn:
      - AppSyncSchema
      - AppSyncDataSource
    Properties:
      ApiId: !GetAtt AppSyncGraphQLApi.ApiId
      TypeName: "Mutation"
      FieldName: "unsubscribe"
      DataSourceName: !GetAtt AppSyncDataSource.Name
      RequestMappingTemplate: |
        #**
        The 'params' key accepts any valid Elasticsearch DSL expression.
        You must replace the <index>, <type>, and <field> placeholders with actual values.
        *#
        {
          "version":"2017-02-28",
          "operation":"POST",
          "path":"/books/_update/${context.arguments.id}",
          "params":{
              "body": {
                  "script" : {
                      "source": "for(int i=0;i<ctx._source.users.size();i++){if(ctx._source.users[i].username==params.username){ctx._source.users.remove(i)}}",
                      "lang": "painless",
                      "params": {
                          "username": $util.toJson(${context.arguments.user.username})
                      }
                  }
              }
          }
        }
      ResponseMappingTemplate: |
        #**
        $context.result contains the full response of the Elasticsearch query.
        You can use this template to return the data from an Elasticsearch
        query that returns a single result.
        *#
        $util.toJson($context.arguments.user)
      Kind: "UNIT"



Outputs:

  AppSyncElasticSearchDomainEndpoint:
    Description: The Amazon Elasticsearch domain's endpoint.
    Value: !Join ["", ["https://", !GetAtt AppSyncElasticSearch.DomainEndpoint]]
  AppSyncElasticSearchServiceRole:
    Description: The AppSync service role created with permissions to Amazon Elasticsearch operations.
    Value: !GetAtt AppSyncESServiceRole.Arn

  AppSyncGraphQLApiArn:
    Description: The arn for the AppSyncGraphQLApi.
    Value: !GetAtt AppSyncGraphQLApi.Arn
  AppSyncGraphQLApiId:
    Description: The ApiId for the AppSyncGraphQLApi.
    Value: !GetAtt AppSyncGraphQLApi.ApiId
  AppSyncGraphQLApiEndpoint:
    Description: The endpoint for the GraphQLApi.
    Value: !GetAtt AppSyncGraphQLApi.GraphQLUrl

  AppSyncApiKey:
    Description: The ApiKey for the AppSyncGraphQLApi.
    Value: !Ref AppSyncApiKey

  AppSyncDataSourceArn:
    Description: The arn for AppSyncDataSource.
    Value: !GetAtt AppSyncDataSource.DataSourceArn
  AppSyncDataSourceName:
    Description: The name of the AppSyncDataSource.
    Value: !GetAtt AppSyncDataSource.Name
