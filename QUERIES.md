## Curl the appsync graphql endpoint
```bash
curl -XPOST -H "Content-Type:application/graphql" -H "x-api-key:PUT_KEY_HERE" -d '{"query": "QUERY_FROM_BELOW"}' https://qcz56yyeo5h55bzwy4rcnawcuy.appsync-api.us-east-2.amazonaws.com/graphql
```

```
query Query {
  books(query: "") {
    id, title, users{username, name}, formats{format, link}
  }
}
```

```
mutation Book1{
  book(book: 
  {
    id: "book1",
    title: "Lean Enterprise: How High Performance Organizations Innovate at Scale",
    description: "How well does your organization respond to changing market conditions, customer needs, and emerging technologies when building software-based products? This practical guide presents Lean and Agile principles and patterns to help you move fast at scale—and demonstrates why and how to apply these methodologies throughout your organization, rather than with just one department or team.\n\nThrough case studies, you'll learn how successful enterprises have rethought everything from governance and financial management to systems architecture and organizational culture in the pursuit of radically improved performance. Adopting Lean will take time and commitment, but it's vital for harnessing the cultural and technical forces that are accelerating the rate of innovation.\n\nDiscover how Lean focuses on people and teamwork at every level, in contrast to traditional management practices.\nApproach problem-solving experimentally, by exploring solutions, testing assumptions, and getting feedback from real users.\nLead and manage large-scale programs in a way that empowers employees, increases the speed and quality of delivery, and lowers costs.\nLearn how to implement ideas from the DevOps and Lean Startup movements even in complex, regulated environments.",
    pages: 352,
    authors: [
        "Jez Humble",
        "Joanne Molesky",
        "Barry O'Reilly"
    ],
    publishYear: 2014,
    tags: ["Lean", "Devops", "Development Methodologies", "Business"],
    formats: [
        {
            format: BOOK,
            link: "https://www.amazon.com/Lean-Enterprise-Performance-Organizations-Innovate/dp/1449368425"
        },
        {
            format: KINDLE,
            link: "https://www.amazon.com/Lean-Enterprise-Performance-Organizations-Innovate-ebook/dp/B00QL5MSF8"
        }
    ],
    users: [
        {
            username: "pajones",
            name: "Paul Jones",
            quarter: "1Q2020",
            priority: 1,
            will_lead: true,
            location: "Lehi",
            format: BOOK
        },
        {
            username: "reznik",
            name: "Ilya Reznik",
            quarter: "1Q2020",
            priority: 1,
            will_lead: false,
            location: "San Francisco",
            format: KINDLE
        },
        {
            username: "reznik",
            name: "Ilya Reznik",
            quarter: "4Q2019",
            priority: 2,
            will_lead: false,
            location: "San Francisco",
            format: BOOK
        }
    ],
    suggested_by: "Paul Jones"
}
  ) {
    id
  }
}
```

```
mutation Book2{
  book(book: 
  {
    id: "book2",
    title: "The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win",
    description: "Bill is an IT manager at Parts Unlimited. It's Tuesday morning and on his drive into the office, Bill gets a call from the CEO.\n\nThe company's new IT initiative, code named Phoenix Project, is critical to the future of Parts Unlimited, but the project is massively over budget and very late. The CEO wants Bill to report directly to him and fix the mess in ninety days or else Bill's entire department will be outsourced.\n\nWith the help of a prospective board member and his mysterious philosophy of The Three Ways, Bill starts to see that IT work has more in common with manufacturing plant work than he ever imagined. With the clock ticking, Bill must organize work flow streamline interdepartmental communications, and effectively serve the other business functions at Parts Unlimited.\n\nIn a fast-paced and entertaining style, three luminaries of the DevOps movement deliver a story that anyone who works in IT will recognize. Readers will not only learn how to improve their own IT organizations, they'll never view IT the same way again.",
    pages: 345,
    authors: [
        "Gene Kim",
        "Kevin Behr",
        "George Spafford"
    ],
    publishYear: 2013,
    tags: ["Lean", "Kanban", "Devops", "Development Methodologies", "Business"],
    formats: [
        {
            format: BOOK,
            link: "https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262592"
        },
        {
            format: KINDLE,
            link: "https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business-ebook/dp/B078Y98RG8"
        },
        {
            format: AUDIBLE,
            link: "https://www.amazon.com/Phoenix-Project-Helping-Business-Anniversary/dp/B00VATFAMI"
        }
    ],
    users: [
        {
            username: "pajones",
            name: "Paul Jones",
            quarter: "1Q2020",
            priority: 2,
            will_lead: true,
            location: "Lehi",
            format: BOOK
        },
        {
            username: "reznik",
            name: "Ilya Reznik",
            quarter: "1Q2020",
            priority: 2,
            will_lead: false,
            location: "San Francisco",
            format: KINDLE
        },
        {
            username: "foong",
            name: "Adrian Foong",
            quarter: "1Q2020",
            priority: 1,
            will_lead: false,
            location: "Lehi",
            format: BOOK
        }
    ],
    suggested_by: "Paul Jones"
}
  ) {
    id
  }
}
```

```
mutation Mutation {
  subscribe(id: "book1", user: 
    {
      username: "pajones",
      name: "Paul Jones",
      quarter: "1Q2020",
      priority: 1,
      will_lead: true,
      location: "Lehi",
      format: BOOK
    }) {
    username
  }
}
```

```
mutation Mutation {
  unsubscribe(id: "book1", user: 
    {
      username: "pajones",
      name: "Paul Jones",
      quarter: "1Q2020",
      priority: 1,
      will_lead: true,
      location: "Lehi",
      format: BOOK
    }) {
    username
  }
}
```